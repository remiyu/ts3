TeamSpeak3 Server Container
===========================

Docker
------

Check this project out on dockerhub_ or ``docker pull jwhet/ts3``.
More information on getting started with docker here_.

.. _dockerhub: https://hub.docker.com/r/jwhet/ts3/
.. _here: https://docs.docker.com/engine/getstarted/step_one/#/step-1-get-docker


Building the Container
----------------------
After cloning the repo:

::

    docker build -t ${dockerhub}/${image-name} .
    # OR
    docker-compose build

Example:

::

    docker build -t jwhet/ts3 .

Running the Container
---------------------
The following will run (and download) the container with the
name ``ts3`` and the default ports mapped:

::

    # Starting
    docker run -d -p 9987:9987/udp -p 10011:10011 -p 30033:30033 -v ts3:/server --name ts3 jwhet/ts3
    # Stopping
    docker stop ts3

Using Compose
-------------
Optionally, you can use `docker-compose` while in the repo
to configure the container automatically.

::

    # Starting (in the background; foreground remove '-d')
    docker-compose up -d
    # Stoping
    docker-compose down

ServerAdmin Password
--------------------
**ServerAdmin** password is set on the first run of the container
and can be read from the logs (use `docker logs ts3`). Please
note the **ServerAdmin** details if you plan on making server
changes. It may be easier to run ``docker-compose up`` in the
foreground for first-run to gather this data.


Contributing
------------
1. Clone the repo.
2. Create a new branch.
3. Submit a merge request.

